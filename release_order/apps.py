from django.apps import AppConfig


class ReleaseOrderConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'release_order'
