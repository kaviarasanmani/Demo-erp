import decimal

from datetime import timedelta

from django.contrib.auth.models import User
from django.db import models

from client.models import Client
from team.models import Team

class Release_order(models.Model):

    # invoice_number = models.IntegerField(default=1)
    client_name = models.CharField(max_length=255)
    client_email = models.CharField(max_length=255)
    client_org_number = models.CharField(max_length=255, blank=True, null=True)
    client_address1 = models.CharField(max_length=255, blank=True, null=True)
    client_address2 = models.CharField(max_length=255, blank=True, null=True)
    client_zipcode = models.CharField(max_length=255, blank=True, null=True)
    client_place = models.CharField(max_length=255, blank=True, null=True)
    client_country = models.CharField(max_length=255, blank=True, null=True)
    client_contact_person = models.CharField(max_length=255, blank=True, null=True)
    client_contact_reference = models.CharField(max_length=255, blank=True, null=True)
    sender_reference = models.CharField(max_length=255, blank=True, null=True)
    # invoice_type = models.CharField(max_length=20, choices=CHOICES_TYPE, default=INVOICE)
    # due_days = models.IntegerField(default=14)
    # is_credit_for = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True)
    # is_credited = models.BooleanField(default=False)
    is_sent = models.BooleanField(default=False)
    # is_paid = models.BooleanField(default=False)
    # invoice = models.ForeignKey(Invoice, related_name='items', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    quantity = models.IntegerField(default=1)
    unit_price = models.DecimalField(max_digits=8, decimal_places=2)
    net_amount = models.DecimalField(max_digits=8, decimal_places=2)
    vat_rate = models.IntegerField(default=0)
    discount = models.IntegerField(default=0)
    bankaccount = models.CharField(max_length=266, blank=True, null=True)
    gross_amount = models.DecimalField(max_digits=6, decimal_places=2)
    vat_amount = models.DecimalField(max_digits=6, decimal_places=2)
    net_amount = models.DecimalField(max_digits=6, decimal_places=2)
    discount_amount = models.DecimalField(max_digits=6, decimal_places=2)
    team = models.ForeignKey(Team, related_name='release_order', on_delete=models.CASCADE)
    client = models.ForeignKey(Client, related_name='release_order', on_delete=models.CASCADE)
    created_by = models.ForeignKey(User, related_name='created_release_order', on_delete=models.CASCADE)
    modified_by = models.ForeignKey(User, related_name='modified_release_order', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    

    def get_gross_amount(self):
        vat_rate = decimal.Decimal(self.vat_rate/100)
        return self.net_amount + (self.net_amount * vat_rate)

    class Meta:
        ordering = ['-created_at']
    
    

    